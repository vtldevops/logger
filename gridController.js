//(function($) {
var endpoint = "http://lmsapi-env.us-east-1.elasticbeanstalk.com/log/mailgun";
var newEndpoint;

gridController = {
    loadData: function(filter) {

        //endpoint + key + type
        var d = $.Deferred();
        if (callCount === 0) {
            $.ajax({
                type: "POST",
                url: newEndpoint,
                dataType: "json",
                data: filter,
            }).done(function(response, data) {
                var dataArray = [];
                if (response.data) {
                    var data = response.data;
                    var gridFieldsArray = [];
                    var fieldsArray = [{
                        name: "id",
                        type: "text",
                        visible: false
                    }];

                    for (var field in data) {
                        for (var key in data[field].body) {

                            if (gridFieldsArray.indexOf(key) < 0) {
                                gridFieldsArray.push(key);

                                var obj = {
                                    name: String(key),
                                    type: "text"
                                };

                                fieldsArray.push(obj);
                            }
                        }
                    }

                    fieldsArray.push(
                        {
                            type: "control",
                            name: "preview",
                            itemTemplate: function() {
                                return $("<button>").attr({
                                    "type": "button",
                                    "data-toggle": "modal",
                                    "data-target": "#myModal"
                                })
                                .text("Preview")
                                .on("click", function () {
                                    showPreview();
                                });
                            }
                        },
                        {
                            type: "control",
                            name: "control",
                            modeSwitchButton: false,
                            editButton: false,
                            deleteButton: true,
                    });

                    $("#jsGrid").jsGrid({
                        fields: fieldsArray
                    });

                    for (var x in data) {
                        data[x].body.id = x;
                        dataArray.push(data[x].body);
                    }

                    for (var y in dataArray) {

                        for (var z in dataArray[y]) {

                            if (typeof(dataArray[y][z]) === "object") {
                                
                                                                /*
                                //replace object with its keys as a string
                                var objString = "";
                                
                                for (var w in dataArray[y][z]){

                                    var objSubString = "";
                                    objSubString += String(w) + " : " + String(dataArray[y][z][w]);
                                    objString += "\n" + objSubString;
                                }
                                */
                                dataArray[y][z] = '<a href="#">...</a>';

                            }
                        }
                    }
                } else {
                    dataArray = [];
                }
                return d.resolve(dataArray);
            });
            callCount = 1;
        }
        return d;
    },

    deleteItem: function(item) {

        responseItem = $.ajax({
            type: "DELETE ",
            url: endpoint + "id/" + String(item.id),
            dataType: "json",
            mimeType: "application/json",
            contentType: "application/json",
            processData: false,
            data: item.id
        });
    }
};
//})(jQuery);